# LP P1 RapLab

## Description

RapLab is a web application for writing, visualizing and analyzing rap songs.

## Team

Myself

## Technology

Angular, Google Cloud Platform (Firebase), RxJS, TypeScript, JavaScript, HTML, SCSS, CSS

## My Responsibility

I was responsible for everything.

## Difficulties faced and lessons learned

Get in touch with shadow DOM problems as well as responsive and reactive web design issus.

## How To

1.  Open website https://raplab-raplab.web.app/home
2.  Go to user tab and login as anonymous user with the ”Anonymous Login” button
3.  Go to projects tab and click through the provided projects
4.  Double-click on a table cell starts the music at the cell position
5.  Duplicate a public project
    * Or create a new private project with the provided sound and midi files
    * To do so, go to private projects
    * Press ”+” button
    * Type in project name and press ”+” button again
    * Open the new created project
    * Go to audio tab
    * Upload sound and midi files
    * Set a file to main, to synchronize it with the table
6.  Go to private projects
7.  Open a private project
8.  Write lyrics